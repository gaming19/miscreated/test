--[[-- remove the block comment to enable

-- sample weather mod
-- .. place as many files in the mods folder as you like they all will be executed in same order on client/server resulting in the same indicies etc.

-- define a new weather pattern
local newWeather = {
  name="NewClearSky",
  probability=1, 
  danger=-8,
  duration={15, 19},
  audio=
  {
    {
      class="AudioAreaAmbience",
      trigger="Play_wind",
      rtpc="wind_speed",
      rtpcValue=0.3,
    },
  },
}

-- Add new weather to patterns at the end of list
table.insert(Weather.patterns, newWeather)

-- Now adjust the weather chance so they total 100% again (substract from ClearSky/pattern 1)
local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky") -- using function from common.lua)
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability

-- to check if your mod has taken effect you can run wm_startPattern without params which will list all patterns and their probabilities

]]--



--------------------------------------------------------------------------------------------

      local newWeather = {
       name="SunsetDream", -- Intro pattern
      probability=1,
      danger=5,
      dangerlimit=40,
      duration={4, 6},
	  ramp={0.2, 0.4}, -- use 20% fade-in and 40% fade-out for fog
	  todlimit={17,18}, -- pattern can only spawn between those times
      modifiers = { -- values added to current baseline (faded in and out)
        --               outside, inside, underground, underwater
        humidity=       {-0.1,     -0.1,    -0.1,        },
        light=          {0.2,    0.1,               },
        temperature=    {2,       2,                  },
      },
      entities=
      {
       {
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.fog.fog_light"}, 
            PulsePeriod= {20}, 
            Strength={0.85}, -- Strength={0.0, 1.0},
          },
        },
      },
      audio={},
      tod=
      {
        [etod.PARAM_SUN_INTENSITY]=                 {0.8, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10, for intro have it only slightly lower

        -- Non-Volumetric Fog
        [etod.PARAM_VOLFOG_GLOBAL_DENSITY]=         {1.5, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_DENSITY]=                {0.5, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_FOG_COLOR_MULTIPLIER]=          {0.3},
        [etod.PARAM_FOG_COLOR2_MULTIPLIER]=         {0.3},

        [etod.PARAM_FOG_COLOR]=                     {{ x=147/255, y=84/255, z=141/255 }, constraint=econ.DARKEN }, -- bottom -- previously orange x=253/255, y=166/255, z=87/255
        [etod.PARAM_FOG_COLOR2]=                    {{ x=147/255, y=84/255, z=141/255 }, constraint=econ.DARKEN }, -- top
        [etod.PARAM_VOLFOG_RAMP_INFLUENCE]=         {0.01, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_HEIGHT_OFFSET]=          {0.1, undergroundFactor=_uff},
        [etod.PARAM_VOLFOG_FINAL_DENSITY_CLAMP]=        {0.7, undergroundFactor=_ufd, insideFactor=_ifd}, -- allow look through

        -- color grading
      },
    }

--------------------------------------------------------------------------------------------

table.insert(Weather.patterns, newWeather)

local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSkyWindy")
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability