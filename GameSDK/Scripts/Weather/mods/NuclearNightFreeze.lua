--[[-- remove the block comment to enable

-- sample weather mod
-- .. place as many files in the mods folder as you like they all will be executed in same order on client/server resulting in the same indicies etc.

-- define a new weather pattern
local newWeather = {
  name="NewClearSky",
  probability=1, 
  danger=-8,
  duration={15, 19},
  audio=
  {
    {
      class="AudioAreaAmbience",
      trigger="Play_wind",
      rtpc="wind_speed",
      rtpcValue=0.3,
    },
  },
}

-- Add new weather to patterns at the end of list
table.insert(Weather.patterns, newWeather)

-- Now adjust the weather chance so they total 100% again (substract from ClearSky/pattern 1)
local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky") -- using function from common.lua)
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability

-- to check if your mod has taken effect you can run wm_startPattern without params which will list all patterns and their probabilities

]]--



--------------------------------------------------------------------------------------------

      local newWeather = {
      name="NuclearNightFreeze",
      probability=2,
      danger=45,
      dangerlimit=40,
      duration={3, 4},
      continue={
        ["NuclearFlashFreeze_Peak"] = 100, -- Continue pattern starts at fade out of this pattern, so blending works
      }, 
      ramp={0.3, 0.05}, -- use 30% fade-in and 5% fade-out
	  todlimit={0,7}, -- pattern can only spawn between those times
      modifiers = { -- values added to current baseline (faded in and out)
        --              outside, inside, underground, underwater
        humidity=       {0.3,       0.2,    0.15,         },
        light=          {-0.4,      -0.25,                },
        temperature=    {-10,       -5,       -3,    -6   },
        gas_radiation=  {0.0075,     0.002,                },
        rain_radiation= {0.015,      0.01,                },
        geigercounter=  {1,          0.75,    0.0,    1  },
      },
      entities={
        {
          class="Snow",
          pos={ x=4096, y=4096, z=450 }, -- center of map
          properties=
          {
            bEnabled= {1},
            fRadius= {8000}, -- whole map
            ["SnowFall.nSnowFlakeCount"]= {0},
            ["Surface.fSnowAmount"]= {0.0},
            ["Surface.fFrostAmount"]= {0.0,0.8,0.8},
            ["Surface.fSurfaceFreezing"]= {0.0,0.05,0.05},
          },
        },
        {
          class="WindArea", -- cant be interpolated unless physicalized per frame, but not really needed
          properties=
          {
            --        start,  full(default=start), end(default=start)
            bActive=    {1},
            bEllipsoidal= {0},
            Speed=      {1.5},
            Size=     {{ x=200,y=200,z=200 }},
          },
          OnCustomUpdate=windMover,
        },
      },
      audio={
        {
          class="AudioAreaAmbience",
          trigger="Play_wind",
          rtpc="wind_speed",
          rtpcValue=2.0,
        },
        {
          class="AudioAreaAmbience",
          trigger="Play_nuclear_winter",
          rtpc="freeze_strength",
          rtpcValue=1.0,
        },
      },
      tod=
      {
        -- Sun
        [etod.PARAM_SUN_INTENSITY]=                 {0.1, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10, for intro have it only slightly lower
        [etod.PARAM_FOG_RADIAL_COLOR]=              {{ x=48/255, y=67/255, z=110/255 } }, -- saturated grey blue
        [etod.PARAM_FOG_RADIAL_COLOR_MULTIPLIER]=   {0.734},

        -- Colorgrading
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_COLOR]=    {{ x=4/255, y=2/255, z=62/255 }, }, -- dark blue
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_DENSITY]=  {0.25},

        -- Filters
        [etod.PARAM_COLORGRADING_DOF_BLURAMOUNT]=   {0.2, method=emix.ADDITIVE},
      },
    }

--------------------------------------------------------------------------------------------

table.insert(Weather.patterns, newWeather)

local patternToAdjust = FindInTable(Weather.patterns, "name", "NuclearFlashFreeze")
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability