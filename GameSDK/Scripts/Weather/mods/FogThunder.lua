--[[-- remove the block comment to enable

-- sample weather mod
-- .. place as many files in the mods folder as you like they all will be executed in same order on client/server resulting in the same indicies etc.

-- define a new weather pattern
local newWeather = {
  name="NewClearSky",
  probability=1, 
  danger=-8,
  duration={15, 19},
  audio=
  {
    {
      class="AudioAreaAmbience",
      trigger="Play_wind",
      rtpc="wind_speed",
      rtpcValue=0.3,
    },
  },
}

-- Add new weather to patterns at the end of list
table.insert(Weather.patterns, newWeather)

-- Now adjust the weather chance so they total 100% again (substract from ClearSky/pattern 1)
local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky") -- using function from common.lua)
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability

-- to check if your mod has taken effect you can run wm_startPattern without params which will list all patterns and their probabilities

]]--



--------------------------------------------------------------------------------------------

      local newWeather = {
      name="FogThunder",
      probability=1,
      danger=15,
      dangerlimit=40,
      continue={
        ["MediumRain"] = 13,
		["MediumFog"] = 20,
		["HeavyRain"] = 13,
		["StormyDistantThunder"] = 54,
      }, 
      duration={5, 12},
	  ramp={0.2, 0.4}, 
      modifiers = { -- values added to current baseline (faded in and out)
        --          outside, inside, underground, underwater
        humidity=   {0.3,      0.2,              },
        light=      {-0.4,    -0.1,               },
        temperature={-5,      -2,                },
        wind=       {8,       3,     2,          },
      },
	  
	  entities=
      {
        
        {
          class="Lightning",
          properties=
          {
            bActive= {1},
            fDistance= {800,760}, 
            bRelativeToPlayer= {1},
            ["Timing.fDelay"]= {25},
            ["Timing.fDelayVariation"]= {0.75},
            ["Timing.fLightningDuration"]= {0.1},
            ["Effects.LightIntensity"]= {0.025},
            ["Effects.ParticleEffect"]= {"weather.lightning.lightningbolt1"},
            ["Effects.ParticleScale"]= {0.3, 0.2},
            ["Effects.SkyHighlightVerticalOffset"]= {300,250},
            ["Effects.SkyHighlightMultiplier"]= {0.015,0.01},
            ["Audio.audioTriggerPlayTrigger"]= {"Play_thunder"},
          },
        },
        {
          class="Lightning",
          properties=
          {
            bActive= {1},
            fDistance= {750,800},
            bRelativeToPlayer= {1},
            ["Timing.fDelay"]= {20},
            ["Timing.fDelayVariation"]= {0.6},
            ["Timing.fLightningDuration"]= {0.1},
            ["Effects.LightIntensity"]= {0.01}, 
            ["Effects.ParticleEffect"]= {"weather.lightning.lightningbolt1"},
            ["Effects.ParticleScale"]= {0.5}, 
            ["Effects.SkyHighlightVerticalOffset"]= {250,400},
            ["Effects.SkyHighlightMultiplier"]= {0.01,0.015},
            ["Audio.audioTriggerPlayTrigger"]= {"Play_thunder"},
          },
        },
		{
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.fog.fog_medium"}, 
            PulsePeriod= {20}, 
            Strength={1.0}, -- Strength={0.0, 1.0},
          },
        },
      },
      
      audio={},
      tod=
      {
        [etod.PARAM_SUN_INTENSITY]=						{0.2, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10
        [etod.PARAM_FOG_RADIAL_COLOR_MULTIPLIER]=		{0.1},

        -- Non-Volumetric Fog
        [etod.PARAM_VOLFOG_GLOBAL_DENSITY]=				{1.0, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_RAMP_INFLUENCE]=				{0.25, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_HEIGHT_OFFSET]=				{1.0, undergroundFactor=_uff},

        -- Volumetric Fog
        [etod.PARAM_VOLFOG2_GLOBAL_DENSITY]=			{1.0, method=emix.QUART, undergroundFactor=_uff, insideFactor=_iff},

        [etod.PARAM_COLORGRADING_FILTERS_GRAIN]=		{0.3},
        [etod.PARAM_COLORGRADING_DOF_FOCUSRANGE]=		{250, undergroundFactor=_ufd, insideFactor=_ifd},
        [etod.PARAM_COLORGRADING_DOF_BLURAMOUNT]=		{0.2, method=emix.ADDITIVE, undergroundFactor=_ufd, insideFactor=_ifd},
      },
    }

--------------------------------------------------------------------------------------------

table.insert(Weather.patterns, newWeather)

local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSkyWindy")
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability