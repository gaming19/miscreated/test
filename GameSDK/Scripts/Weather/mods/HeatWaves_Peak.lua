--------------------------------------------------------------------------------------------

    local newWeather = {
      name="HeatWaves_Peak",
      probability=0,
      danger=1,
      duration={4, 5},
      continue={
        -- Continue pattern starts at fade out of current pattern (so both are active some time)
        ["ClearSkyWindy"] = 100,
      }, 
      modifiers = { -- values added to current baseline (faded in and out)
        --          outside, inside, underground, underwater
        humidity=   {-0.2,     -0.1,                },
        light=      {0.4,    0.2,               },
		torpidity=  {1.0,     0.5,                    },
        temperature={45,      35,                 },
        wind=       {6,      2,     2,      1   },
      },
      entities=
      {
        
        {
          class="WindArea", -- cant be interpolated unless physicalized per frame, but not really needed
          properties=
          {
            --        start, full(default=start), end(default=start)
            bActive=    {1},
            bEllipsoidal= {0},
            Speed=      {1.9},
            Size=     {{ x=200,y=200,z=200 }},
            Dir=      {{ x=1,y=1,z=-0.5 }},
          },
          OnCustomUpdate=windMover,
        },
        {
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.fog.acid_rain"}, 
            PulsePeriod= {20}, 
            Strength={0.5}, -- Strength={0.0, 1.0},
          },
        },
      },
      audio=
      {
        {
          class="AudioAreaAmbience",
          trigger="Play_wind",
          rtpc="wind_speed",
          rtpcValue=5.0,
        },
		{
          class="AudioAreaAmbience",
          trigger="Play_nuclear_winter",
          rtpc="freeze_strength",
          rtpcValue=0.8,
        },
      },
      tod=
      {
        -- method=default lerp, min=totalmin, max=totalmax
        [etod.PARAM_SUN_INTENSITY]=           {1.2, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10

        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_COLOR]=    {{ x=255/255, y=45/255, z=45/255 }, constraint=econ.BRIGTHEN, },
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_DENSITY]=  {0.45},
        [etod.PARAM_COLORGRADING_FILTERS_GRAIN]=    {0.4},
        [etod.PARAM_COLORGRADING_DOF_FOCUSRANGE]=   {300},
        [etod.PARAM_COLORGRADING_DOF_BLURAMOUNT]=   {0.3, method=emix.ADDITIVE},
      },
    }
	
	table.insert(Weather.patterns, newWeather)

--------------------------------------------------------------------------------------------