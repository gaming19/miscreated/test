--[[-- remove the block comment to enable

-- sample weather mod
-- .. place as many files in the mods folder as you like they all will be executed in same order on client/server resulting in the same indicies etc.

-- define a new weather pattern
local newWeather = {
  name="NewClearSky",
  probability=1, 
  danger=-8,
  duration={15, 19},
  audio=
  {
    {
      class="AudioAreaAmbience",
      trigger="Play_wind",
      rtpc="wind_speed",
      rtpcValue=0.3,
    },
  },
}

-- Add new weather to patterns at the end of list
table.insert(Weather.patterns, newWeather)

-- Now adjust the weather chance so they total 100% again (substract from ClearSky/pattern 1)
local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky") -- using function from common.lua)
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability

-- to check if your mod has taken effect you can run wm_startPattern without params which will list all patterns and their probabilities

]]--



--------------------------------------------------------------------------------------------

      local newWeather = {
      name="FrostFog",
      probability=1,
      danger=17,
      dangerlimit=40,
      duration={5, 12},
      continue={
        ["Snow"] = 45,
		["HeavyFog"] = 45,
		["FrostFall"] = 10,
      }, 
	  ramp={0.4, 0.6},
      modifiers = { -- values added to current baseline (faded in and out)
        --          outside, inside, underground, underwater
        humidity=       {0.3,       0.2,    0.15,         },
        light=          {-0.4,      -0.25,                },
        temperature=    {-25,       -12,      -6,    -8   },
      },
	  
	  entities={
        {
          class="Snow",
          pos={ x=4096, y=4096, z=450 }, -- center of map
          properties=
          {
            bEnabled= {1},
            fRadius= {8000}, -- whole map
			["SnowFall.fGravityScale"]= {0.1,1.0,1.0},
            ["SnowFall.nSnowFlakeCount"]= {120,130,3},
            ["Surface.fSnowAmount"]= {0.0,0.0,0.0},
            ["Surface.fFrostAmount"]= {0.0,1.0,1.0},
            ["Surface.fSurfaceFreezing"]= {0.0,0.35,0.3},
          },
		
		  {
          class="WindArea", -- cant be interpolated unless physicalized per frame, but not really needed
          properties=
          {
            --        start,  full(default=start), end(default=start)
            bActive=    {1},
            bEllipsoidal= {0},
            Speed=      {1.5},
            Size=     {{ x=200,y=200,z=200 }},
          },
          OnCustomUpdate=windMover,
		},
		
		{
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.fog.fog_heavy"}, 
            PulsePeriod= {20}, 
            Strength={1.0}, -- Strength={0.0, 1.0},
          },
        },
      },
	  },
	  
      audio={},
      tod=
      {
        [etod.PARAM_SUN_INTENSITY]=						{0.2, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10
        [etod.PARAM_FOG_RADIAL_COLOR_MULTIPLIER]=		{0.1},

        -- Non-Volumetric Fog
        [etod.PARAM_VOLFOG_GLOBAL_DENSITY]=				{3.5, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_RAMP_INFLUENCE]=				{0.01, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_HEIGHT_OFFSET]=				{1.0, undergroundFactor=_uff},

        -- Volumetric Fog
        [etod.PARAM_VOLFOG2_GLOBAL_DENSITY]=			{2.0, method=emix.QUART, undergroundFactor=_uff, insideFactor=_iff},

        [etod.PARAM_COLORGRADING_FILTERS_GRAIN]=		{0.3},
        [etod.PARAM_COLORGRADING_DOF_FOCUSRANGE]=		{250, undergroundFactor=_ufd, insideFactor=_ifd},
        [etod.PARAM_COLORGRADING_DOF_BLURAMOUNT]=		{0.2, method=emix.ADDITIVE, undergroundFactor=_ufd, insideFactor=_ifd},

        [etod.PARAM_SUN_RAYS_VISIBILITY]=				{0.4},
      },
    }
 

--------------------------------------------------------------------------------------------

table.insert(Weather.patterns, newWeather)

local patternToAdjust = FindInTable(Weather.patterns, "name", "TheMist")

patternToAdjust.probability = patternToAdjust.probability - newWeather.probability