--[[-- remove the block comment to enable

-- sample weather mod
-- .. place as many files in the mods folder as you like they all will be executed in same order on client/server resulting in the same indicies etc.

-- define a new weather pattern
local newWeather = {
  name="NewClearSky",
  probability=1, 
  danger=-8,
  duration={15, 19},
  audio=
  {
    {
      class="AudioAreaAmbience",
      trigger="Play_wind",
      rtpc="wind_speed",
      rtpcValue=0.3,
    },
  },
}

-- Add new weather to patterns at the end of list
table.insert(Weather.patterns, newWeather)

-- Now adjust the weather chance so they total 100% again (substract from ClearSky/pattern 1)
local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky") -- using function from common.lua)
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability

-- to check if your mod has taken effect you can run wm_startPattern without params which will list all patterns and their probabilities

]]--



--------------------------------------------------------------------------------------------

      local newWeather = {
       name="DustStorm_Outro", -- Intro pattern
      probability=0,
      danger=1,
      duration={2, 4},
      continue={
        ["ClearSkyWindy"] = 100, -- Continue pattern starts at fade out of this pattern, so blending works
      }, 
      ramp={0.05, 0.6}, -- use 5% fade-in and 60% fade-out (mix into new effect)
      modifiers = { -- values added to current baseline (faded in and out)
        --               outside, inside, underground, underwater
        humidity=       {0.1,     0.1,    0.1,        },
        light=          {-0.2,    -0.1,               },
        temperature=    {2,       2,                  },
      },
      entities={
        {
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.ash.slow_ash"},
            PulsePeriod= {0},
            Strength={1.0}, -- Strength={1.0, 1.0, 0.0},
          },
        },
		
		 {
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.fog.fog_medium"}, 
            PulsePeriod= {20}, 
            Strength={1.0}, -- Strength={0.0, 1.0},
          },
        },
  
        {
          class="WindArea", -- cant be interpolated unless physicalized per frame, but not really needed
          properties=
          {
            --        start,  full(default=start), end(default=start)
            bActive=    {1},
            bEllipsoidal= {0},
            Speed=      {2},
            Size=     {{ x=200,y=200,z=200 }},
          },
          OnCustomUpdate=windMover,
        },
      },
      audio={
        {
          class="AudioAreaAmbience",
          trigger="Play_wind",
          rtpc="wind_speed",
          rtpcValue=3.0,
        },
      },
      tod=
      {
        [etod.PARAM_SUN_INTENSITY]=                 {0.8, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10, for intro have it only slightly lower

        -- Non-Volumetric Fog
        [etod.PARAM_VOLFOG_GLOBAL_DENSITY]=         {1.5, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_DENSITY]=                {0.5, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_FOG_COLOR_MULTIPLIER]=          {0.3},
        [etod.PARAM_FOG_COLOR2_MULTIPLIER]=         {0.3},

        [etod.PARAM_FOG_COLOR]=                     {{ x=180/255, y=140/255, z=45/255 }, constraint=econ.DARKEN }, -- bottom
        [etod.PARAM_FOG_COLOR2]=                    {{ x=255/255, y=200/255, z=50/255 }, constraint=econ.DARKEN }, -- top
        [etod.PARAM_VOLFOG_RAMP_INFLUENCE]=         {0.01, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_HEIGHT_OFFSET]=          {0.1, undergroundFactor=_uff},
        [etod.PARAM_VOLFOG_FINAL_DENSITY_CLAMP]=        {0.9, undergroundFactor=_uff, insideFactor=_iff}, -- allow look through

        -- color grading
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_COLOR]=    {{ x=255/255, y=200/255, z=50/255 }, constraint=econ.BRIGTHEN, },
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_DENSITY]=  {0.15, undergroundFactor=_ufc, insideFactor=_ifc},
      },
    }

--------------------------------------------------------------------------------------------

table.insert(Weather.patterns, newWeather)