--[[-- remove the block comment to enable

-- sample weather mod
-- .. place as many files in the mods folder as you like they all will be executed in same order on client/server resulting in the same indicies etc.

-- define a new weather pattern
local newWeather = {
  name="NewClearSky",
  probability=1, 
  danger=-8,
  duration={15, 19},
  audio=
  {
    {
      class="AudioAreaAmbience",
      trigger="Play_wind",
      rtpc="wind_speed",
      rtpcValue=0.3,
    },
  },
}

-- Add new weather to patterns at the end of list
table.insert(Weather.patterns, newWeather)

-- Now adjust the weather chance so they total 100% again (substract from ClearSky/pattern 1)
local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky") -- using function from common.lua)
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability

-- to check if your mod has taken effect you can run wm_startPattern without params which will list all patterns and their probabilities

]]--



--------------------------------------------------------------------------------------------

      local newWeather = {
      name="MediumRainThunder",
      probability=1,
      danger=10,
      dangerlimit=40,
      continue={
        ["MediumRain"] = 20,
		["HeavyRain"] = 20,
		["ClearSkyWindy"] = 54,
		["FogThunder"] = 6,
      }, 
      duration={7, 12},
      modifiers = { -- values added to current baseline (faded in and out)
        --          outside, inside, underground, underwater
         humidity=   {0.75,    0.25,   0.2,        },
        rain=       {0.8,                         },
        light=      {-0.7,    -0.3,               },
        temperature={-10,     -4,     -1,         },
        wind=       {80,      6,      4,          },
		},
	  
	  entities=
      {
        BellTriggers[1],BellTriggers[2],BellTriggers[3],
        {
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.fog.fog_rain"}, 
            PulsePeriod= {20}, 
            Strength={1.0}, -- Strength={0.0, 1.0},
          },
        },
        {
          class="Rain",
          properties=
          {
           --            start,  full(default=start), end(default=start)
            bEnabled=       {1},
            fAmount=        {0,   2},
            fDiffuseDarkening=    {0,   0.75},
            fPuddlesAmount=     {0,   2.0},
            fPuddlesMaskAmount=   {0.715},
            fPuddlesRippleAmount= {0,   2.0},
            fRainDropsAmount=   {0,   0.05},
            fRainDropsLighting=   {2.5},
            fRainDropsSpeed=    {1.33}, -- speed cant be interpolated it, because of how rain entity works
            fSplashesAmount=    {0,   1.5},
          },
        },
        {
          class="Lightning",
          properties=
          {
            bActive= {1},
            fDistance= {800,100}, -- lightning closes distance to player and then goes away again
            bRelativeToPlayer= {1},
            ["Timing.fDelay"]= {30},
            ["Timing.fDelayVariation"]= {0.75},
            ["Timing.fLightningDuration"]= {0.3},
            ["Effects.LightIntensity"]= {0.4,1.2}, -- increase intensity as it comes closer
            ["Effects.ParticleEffect"]= {"weather.lightning.lightningbolt1"},
            ["Effects.ParticleScale"]= {4,3}, -- have them a bit larger at distance
            ["Effects.SkyHighlightVerticalOffset"]= {400,80},
            ["Effects.SkyHighlightMultiplier"]= {0.3,1},
            ["Audio.audioTriggerPlayTrigger"]= {"Play_thunder"},
          },
        },
        {
          class="Lightning",
          properties=
          {
            bActive= {1},
            fDistance= {900,400}, -- lightning closes distance to player and then goes away again
            bRelativeToPlayer= {1},
            ["Timing.fDelay"]= {33},
            ["Timing.fDelayVariation"]= {0.6},
            ["Timing.fLightningDuration"]= {0.2},
            ["Effects.LightIntensity"]= {0.4,1.2}, -- increase intensity as it comes closer
            ["Effects.ParticleEffect"]= {"weather.lightning.lightningbolt2"},
            ["Effects.ParticleScale"]= {4,3}, -- have them a bit larger at distance
            ["Effects.SkyHighlightVerticalOffset"]= {300,60},
            ["Effects.SkyHighlightMultiplier"]= {0.3,1},
            ["Audio.audioTriggerPlayTrigger"]= {"Play_thunder"}, --TODO needs a better sound
          },
        },
        {
          class="WindArea", -- cant be interpolated unless physicalized per frame, but not really needed
          properties=
          {
            -- 				start, 	full(default=start), end(default=start)
            bActive=		{1},
            bEllipsoidal=	{0},
            Speed=			{2},
            Size=			{{ x=200,y=200,z=200 }},
          },
          OnCustomUpdate=windMover,
        },
      },
      
      audio=
      {
        {
          class="AudioAreaAmbience",
          trigger="Play_wind",
          rtpc="wind_speed",
          rtpcValue=0.7,
        },
      },
      tod=
      {
        -- method=default lerp, min=totalmin, max=totalmax
        [etod.PARAM_SUN_INTENSITY]=           {0.2, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10

        -- Non-Volumetric Fog
        [etod.PARAM_FOG_RADIAL_COLOR_MULTIPLIER]=   {0.1},
        [etod.PARAM_VOLFOG_GLOBAL_DENSITY]=       {0.4, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_RAMP_INFLUENCE]=       {0.1, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_FOG_COLOR_MULTIPLIER]=        {0.15},
        [etod.PARAM_FOG_COLOR2]=            {{ x=150/255, y=175/255, z=190/255 }, constraint=econ.DARKEN, },
        [etod.PARAM_FOG_COLOR2_MULTIPLIER]=       {0.15},
        [etod.PARAM_VOLFOG_HEIGHT2]=          {4000},
        [etod.PARAM_VOLFOG_DENSITY2]=         {0.3, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_RADIAL_SIZE]=        {0.5, undergroundFactor=_uff, insideFactor=_iff},

        -- Volumetric Fog
        [etod.PARAM_VOLFOG2_HEIGHT2]=         {10000},
        [etod.PARAM_VOLFOG2_GLOBAL_DENSITY]=      {0.5, method=emix.QUART, undergroundFactor=_uff, insideFactor=_iff},

        [etod.PARAM_SKYLIGHT_SUN_INTENSITY_MULTIPLIER]= {15},

		[etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_COLOR]=    {{ x=100/255, y=100/255, z=0/255 }, constraint=econ.BRIGTHEN, },
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_DENSITY]=  {0.2, undergroundFactor=_ufc, insideFactor=_ifc},
        [etod.PARAM_COLORGRADING_FILTERS_GRAIN]=    {0.1},
        [etod.PARAM_COLORGRADING_DOF_FOCUSRANGE]=   {800, undergroundFactor=_ufd, insideFactor=_ifd},
        [etod.PARAM_COLORGRADING_DOF_BLURAMOUNT]=   {0.25, method=emix.ADDITIVE, undergroundFactor=_ufd, insideFactor=_ifd},
		[etod.PARAM_SUN_RAYS_VISIBILITY]=       {0.3},
      },
    }

--------------------------------------------------------------------------------------------

table.insert(Weather.patterns, newWeather)

local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSkyWindy")
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability