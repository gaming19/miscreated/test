--[[-- remove the block comment to enable

-- sample weather mod
-- .. place as many files in the mods folder as you like they all will be executed in same order on client/server resulting in the same indicies etc.

-- define a new weather pattern
local newWeather = {
  name="NewClearSky",
  probability=1, 
  danger=-8,
  duration={15, 19},
  audio=
  {
    {
      class="AudioAreaAmbience",
      trigger="Play_wind",
      rtpc="wind_speed",
      rtpcValue=0.3,
    },
  },
}

-- Add new weather to patterns at the end of list
table.insert(Weather.patterns, newWeather)

-- Now adjust the weather chance so they total 100% again (substract from ClearSky/pattern 1)
local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky") -- using function from common.lua)
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability

-- to check if your mod has taken effect you can run wm_startPattern without params which will list all patterns and their probabilities

]]--



--------------------------------------------------------------------------------------------

      local newWeather = {
      name="HeatWaves",
      probability=2,
      danger=35,
      dangerlimit=40,
      continue={
        ["HeatWaves_Peak"] = 100,
      }, 
      duration={2, 3},
	  todlimit={7,18}, -- pattern can only spawn between those times
      modifiers = { -- values added to current baseline (faded in and out)
        --          outside, inside, underground, underwater
        humidity=   {-0.3,     -0.2,                },
        light=      {0.3,    0.2,               },
		torpidity=  {0.1,                         },
        temperature={35,      20,                 },
        wind=       {6,      2,     2,      1   },
      },
	  
	  entities=
      {
        WeatherSirenTriggers[1],WeatherSirenTriggers[2],WeatherSirenTriggers[3],WeatherSirenTriggers[4],WeatherSirenTriggers[5],WeatherSirenTriggers[6],WeatherSirenTriggers[7],WeatherSirenTriggers[8],WeatherSirenTriggers[9],WeatherSirenTriggers[10],WeatherSirenTriggers[11],
        BellTriggers[1],BellTriggers[2],BellTriggers[3],
		},
      
      audio=
      {
        {
          class="AudioAreaAmbience",
          trigger="Play_wind",
          rtpc="wind_speed",
          rtpcValue=1.0,
        },
		{
          class="AudioAreaAmbience",
          trigger="Play_nuclear_winter",
          rtpc="freeze_strength",
          rtpcValue=0.25,
        },
      },
      tod=
      {
        -- method=default lerp, min=totalmin, max=totalmax
        [etod.PARAM_SUN_INTENSITY]=           {1.2, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10

        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_COLOR]=    {{ x=255/255, y=45/255, z=45/255 }, constraint=econ.BRIGTHEN, },
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_DENSITY]=  {0.30},
        [etod.PARAM_COLORGRADING_FILTERS_GRAIN]=    {0.2},
        [etod.PARAM_COLORGRADING_DOF_FOCUSRANGE]=   {300},
        [etod.PARAM_COLORGRADING_DOF_BLURAMOUNT]=   {0.3, method=emix.ADDITIVE},
      },
    }

--------------------------------------------------------------------------------------------

table.insert(Weather.patterns, newWeather)

local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky")
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability