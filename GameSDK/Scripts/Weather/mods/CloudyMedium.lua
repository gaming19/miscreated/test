--[[-- remove the block comment to enable

-- sample weather mod
-- .. place as many files in the mods folder as you like they all will be executed in same order on client/server resulting in the same indicies etc.

-- define a new weather pattern
local newWeather = {
  name="NewClearSky",
  probability=1, 
  danger=-8,
  duration={15, 19},
  audio=
  {
    {
      class="AudioAreaAmbience",
      trigger="Play_wind",
      rtpc="wind_speed",
      rtpcValue=0.3,
    },
  },
}

-- Add new weather to patterns at the end of list
table.insert(Weather.patterns, newWeather)

-- Now adjust the weather chance so they total 100% again (substract from ClearSky/pattern 1)
local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky") -- using function from common.lua)
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability

-- to check if your mod has taken effect you can run wm_startPattern without params which will list all patterns and their probabilities

]]--



--------------------------------------------------------------------------------------------

      local newWeather = {
      name="CloudyMedium",
      probability=3,
      danger=12,
      dangerlimit=40,
      continue={
        ["MediumRain"] = 10,
		["MediumRainThunder"] = 16,
		["CloudyStormy"] = 10,
		["ClearSkyWindy"] = 64,
      }, 
      duration={5, 10},
      modifiers = { -- values added to current baseline (faded in and out)
        --          outside, inside, underground, underwater
        humidity=   {0.2,      0.1               },
        light=      {-0.2,    -0.1,               },
        temperature={-4,      -1,                },
        wind=       {8,       3,     2,          },
      },
	  
	  entities=
      {
		{
          class="WindArea", -- cant be interpolated unless physicalized per frame, but not really needed
          properties=
          {
            --        start, full(default=start), end(default=start)
            bActive=    {1},
            bEllipsoidal= {0},
            Speed=      {1.9},
            Size=     {{ x=200,y=200,z=200 }},
            Dir=      {{ x=1,y=1,z=-0.5 }},
          },
          OnCustomUpdate=windMover,
        },
      },
      
      audio=
      {
        {
          class="AudioAreaAmbience",
          trigger="Play_wind",
          rtpc="wind_speed",
          rtpcValue=1.0,
        },
      },
      tod=
      {
        -- method=default lerp, min=totalmin, max=totalmax
        [etod.PARAM_SUN_INTENSITY]=           {0.5, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10

        -- Non-Volumetric Fog
        [etod.PARAM_FOG_RADIAL_COLOR_MULTIPLIER]=   {0.1},
        [etod.PARAM_VOLFOG_GLOBAL_DENSITY]=       {0.4, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_RAMP_INFLUENCE]=       {0.1, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_FOG_COLOR_MULTIPLIER]=        {0.15},
        [etod.PARAM_FOG_COLOR2]=            {{ x=150/255, y=175/255, z=190/255 }, constraint=econ.DARKEN, },
        [etod.PARAM_FOG_COLOR2_MULTIPLIER]=       {0.15},
        [etod.PARAM_VOLFOG_HEIGHT2]=          {4000},
        [etod.PARAM_VOLFOG_DENSITY2]=         {0.3, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_RADIAL_SIZE]=        {0.5, undergroundFactor=_uff, insideFactor=_iff},

        -- Volumetric Fog
        [etod.PARAM_VOLFOG2_HEIGHT2]=         {10000},
        [etod.PARAM_VOLFOG2_GLOBAL_DENSITY]=      {0.35, method=emix.QUART, undergroundFactor=_uff, insideFactor=_iff},

        [etod.PARAM_SKYLIGHT_SUN_INTENSITY_MULTIPLIER]= {15},

		[etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_COLOR]=    {{ x=150/255, y=150/255, z=50/255 }, constraint=econ.BRIGTHEN, },
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_DENSITY]=  {0.2, undergroundFactor=_ufc, insideFactor=_ifc},
        [etod.PARAM_COLORGRADING_FILTERS_GRAIN]=    {0.1},
        [etod.PARAM_COLORGRADING_DOF_FOCUSRANGE]=   {800, undergroundFactor=_ufd, insideFactor=_ifd},
        [etod.PARAM_COLORGRADING_DOF_BLURAMOUNT]=   {0.25, method=emix.ADDITIVE, undergroundFactor=_ufd, insideFactor=_ifd},
		[etod.PARAM_SUN_RAYS_VISIBILITY]=       {0.5},
      },
    }

--------------------------------------------------------------------------------------------

table.insert(Weather.patterns, newWeather)

local patternToAdjust = FindInTable(Weather.patterns, "name", "ClearSky")
patternToAdjust.probability = patternToAdjust.probability - newWeather.probability